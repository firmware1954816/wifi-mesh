
## Wifi Mesh Network
This project demonstrates how to use ESP WIFI MESH.
Project is confugurable, user can configure number of layers, number of AP of each node, router ssid, router password and Mqtt broker url.
Root node will connect to router and will turn on its onboard led.
Root node after pressing boot button will get the mac address of all the nodes in its subnetwok and will send the message buffer to all the nodes in the subnet excluding itself.
Root node & Non-Root Nodes communicate using low level mesh send/receive API's to exchange data.
After Pressing boot button of any root and non root node will send perform internal communication and send data to nodes under its subnetwork, excluding itself.

 ## MQTT is implemented in every node.
To show the working of MQTT a button task is created. In this task, we check:
If the device is root, it turns on its onboard LED
if it's non-root, we check if the BOOT button is pressed if pressed device publishes a message to the MQTT topic /topic/ip_mesh/keypressed. The message contains MAC address the device that publishes MQTT messages.
Every node subscribes to that topic including the node that publishes it.
Every Node subscribes to button topic. Except Root node every other node Publush data when button is pressed. It publishes its mac address.

## Configure the project
Open the project configuration menu (idf.py menuconfig) to configure
- The mesh network channel, 
- Router SSID, Router password 
- Mesh numher of layers, number of AP of each node.



## Build and Flash
Build the project and flash it to multiple boards forming a mesh network, then run monitor tool to view serial output:

idf.py -p PORT flash monitor

(To exit the serial monitor, type Ctrl-].)
See the Getting Started Guide for full steps to configure and use ESP-IDF to build projects.