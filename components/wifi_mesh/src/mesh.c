#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_wifi.h"
#include "esp_system.h"

#include "esp_event.h"
#include "esp_log.h"
#include "esp_mac.h"

#include "esp_mesh.h"
#include "esp_mesh_internal.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include <lwip/sockets.h>

#include "nvs_flash.h"
#include "driver/gpio.h"

#include "sys_config.h"
#include "app.h"
#include "mesh_netif.h"

char mac_address_root_str[50];
mesh_addr_t route_table[CONFIG_MESH_ROUTE_TABLE_SIZE];

static const char *TAG = "mesh";
static const uint8_t MESH_ID[6] = {0x77, 0x77, 0x77, 0x77, 0x77, 0x77};

static bool is_mesh_connected = false;
static mesh_addr_t mesh_parent_addr;
static int mesh_layer = -1;
static esp_ip4_addr_t s_current_ip;

SemaphoreHandle_t s_route_table_lock = NULL;
EventGroupHandle_t wifi_event_group;
const int CONNECTED_BIT = BIT0;

/**
 * Function prototypes
 */
esp_err_t esp_mesh_rx_start(void);
void MeshEventHandler(void *arg, esp_event_base_t event_base, int32_t event_id, void *pEventData);
void mesh_app_start(void);

void static recv_cb(mesh_addr_t *from, mesh_data_t *data)
{
    if (data->size > 0 && data->data != NULL)
    {
        ESP_LOGD(TAG,"mesh_recv_data:  %s\n\n)", data->data);
        ESP_LOGI(TAG, "mesh_recv_data: %s", data->data);

    }
    else
    {
        // Handle invalid or empty data
        ESP_LOGE(TAG, "Invalid or empty data received");
    }
}
/**
 * Calls reception thread to change messages
 */
esp_err_t esp_mesh_rx_start(void)
{
    static bool is_esp_mesh_rx_started = false;

    s_route_table_lock = xSemaphoreCreateMutex();

    if (!is_esp_mesh_rx_started)
    {
        is_esp_mesh_rx_started = true;
        task_app_create();
    }

    return ESP_OK;
}

void MeshEventHandler(void *arg, esp_event_base_t event_base, int32_t event_id, void *pEventData)
{
    mesh_addr_t id = {
        0,
    };
    static uint8_t last_layer = 0;
    ESP_LOGD(TAG, "esp_event_handler:%ld", event_id);

    switch (event_id)
    {
    case MESH_EVENT_STARTED:
    {
        esp_mesh_get_id(&id);
        ESP_LOGI(TAG, "<MESH_EVENT_STARTED>ID:"MACSTR"", MAC2STR(id.addr));
        is_mesh_connected = false;
        mesh_layer = esp_mesh_get_layer();
    }
    break;
    case MESH_EVENT_STOPPED:
    {
        ESP_LOGI(TAG, "<MESH_EVENT_STOPPED>");
        is_mesh_connected = false;
        mesh_layer = esp_mesh_get_layer();
    }
    break;
    case MESH_EVENT_CHILD_CONNECTED:
    {
        mesh_event_child_connected_t *pChildConnected = (mesh_event_child_connected_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_CHILD_CONNECTED>aid:%d, " MACSTR "", pChildConnected->aid,
                 MAC2STR(pChildConnected->mac));
    }
    break;
    case MESH_EVENT_CHILD_DISCONNECTED:
    {
        mesh_event_child_disconnected_t *pChildDisconnected = (mesh_event_child_disconnected_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_CHILD_DISCONNECTED>aid:%d, " MACSTR "", pChildDisconnected->aid,
                 MAC2STR(pChildDisconnected->mac));
    }
    break;
    case MESH_EVENT_ROUTING_TABLE_ADD:
    {
        mesh_event_routing_table_change_t *pRoutingTable = (mesh_event_routing_table_change_t *)pEventData;
        ESP_LOGW(TAG, "<MESH_EVENT_ROUTING_TABLE_ADD>add %d, new:%d", pRoutingTable->rt_size_change,
                 pRoutingTable->rt_size_new);
    }
    break;
    case MESH_EVENT_ROUTING_TABLE_REMOVE:
    {
        mesh_event_routing_table_change_t *pRoutingTable = (mesh_event_routing_table_change_t *)pEventData;
        ESP_LOGW(TAG, "<MESH_EVENT_ROUTING_TABLE_REMOVE>remove %d, new:%d", pRoutingTable->rt_size_change,
                 pRoutingTable->rt_size_new);
    }
    break;
    case MESH_EVENT_NO_PARENT_FOUND:
    {
        mesh_event_no_parent_found_t *pNoParent = (mesh_event_no_parent_found_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_NO_PARENT_FOUND>scan times:%d", pNoParent->scan_times);
    }
    break;
    case MESH_EVENT_PARENT_CONNECTED:
    {
        mesh_event_connected_t *pConnected = (mesh_event_connected_t *)pEventData;

        esp_mesh_get_id(&id);                                           // id of current node
        mesh_layer = pConnected->self_layer;                            // layer of current node
        memcpy(&mesh_parent_addr.addr, pConnected->connected.bssid, 6); // mac of parent to which device is connected
        ESP_LOGI(TAG,
                 "<MESH_EVENT_PARENT_CONNECTED>layer:%d-->%d, parent:" MACSTR "%s, ID:" MACSTR "",
                 last_layer, mesh_layer, MAC2STR(mesh_parent_addr.addr),
                 esp_mesh_is_root() ? "<ROOT>" : (mesh_layer == 2) ? "<layer2>"
                                                                   : "",
                 MAC2STR(id.addr));
        last_layer = mesh_layer;
        is_mesh_connected = true;
        mesh_netifs_start(esp_mesh_is_root());
    }
    break;
        /**
         * Parent desconnection event
         */
    case MESH_EVENT_PARENT_DISCONNECTED:
    {
        mesh_event_disconnected_t *pDisconnected = (mesh_event_disconnected_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_PARENT_DISCONNECTED>reason:%d", pDisconnected->reason);
        mesh_layer = esp_mesh_get_layer();
        is_mesh_connected = false;
        mesh_netifs_stop();
    }
    break;

    /**
     * Layer change event
     */
    case MESH_EVENT_LAYER_CHANGE:
    {
        mesh_event_layer_change_t *pLayerChange = (mesh_event_layer_change_t *)pEventData;
        mesh_layer = pLayerChange->new_layer;
        ESP_LOGI(TAG, "<MESH_EVENT_LAYER_CHANGE>layer:%d-->%d%s", last_layer, mesh_layer,
                 esp_mesh_is_root() ? "<ROOT>" : (mesh_layer == 2) ? "<layer2>"
                                                                   : "");
        last_layer = mesh_layer;
    }
    break;

    /**
     * Root address event
     */
    case MESH_EVENT_ROOT_ADDRESS:
    {
        mesh_event_root_address_t *pRootAddress = (mesh_event_root_address_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_ADDRESS>root address:" MACSTR "", MAC2STR(pRootAddress->addr));

        if (esp_mesh_is_root())
        {
            uint8_t chipid[20];
            esp_efuse_mac_get_default(chipid);
            snprintf(mac_address_root_str, sizeof(mac_address_root_str), "" MACSTR "", MAC2STR(chipid));
        }
    }
    break;
    case MESH_EVENT_VOTE_STARTED:
    {
        mesh_event_vote_started_t *pVoteStarted = (mesh_event_vote_started_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_VOTE_STARTED>attempts:%d, reason:%d, rc_addr:" MACSTR "",
                 pVoteStarted->attempts, pVoteStarted->reason, MAC2STR(pVoteStarted->rc_addr.addr));
    }
    break;
    case MESH_EVENT_VOTE_STOPPED:
        ESP_LOGI(TAG, "<MESH_EVENT_VOTE_STOPPED>");
        break;
    /**
     * Software forced request for root exchange
     */
    case MESH_EVENT_ROOT_SWITCH_REQ:
    {
        mesh_event_root_switch_req_t *pSwitchRequest = (mesh_event_root_switch_req_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_SWITCH_REQ>reason:%d, rc_addr:" MACSTR "", pSwitchRequest->reason,
                 MAC2STR(pSwitchRequest->rc_addr.addr));
        break;
    }
    /**
     * Callback acknowledgemnts
     */
    case MESH_EVENT_ROOT_SWITCH_ACK:
    {
        /* new root */
        mesh_layer = esp_mesh_get_layer();
        esp_mesh_get_parent_bssid(&mesh_parent_addr);
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_SWITCH_ACK>layer:%d, parent:" MACSTR "", mesh_layer,
                 MAC2STR(mesh_parent_addr.addr));
        break;
    }
    /**
     * Messages sent by root can be addressed to an external IP.
     * When we use this mesh stack feature, this event will be used
     * in notification of states (toDS - for DS (distribute system))
     */
    case MESH_EVENT_TODS_STATE:
    {
        mesh_event_toDS_state_t *pToDsState = (mesh_event_toDS_state_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_TODS_REACHABLE>state:%d", *pToDsState);
        break;
    }
    /**
     * MESH_EVENT_ROOT_FIXED forces the child device to maintain the same settings as the
     * parent device on the mesh network;
     */
    case MESH_EVENT_ROOT_FIXED:
    {
        mesh_event_root_fixed_t *pRootFixed = (mesh_event_root_fixed_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_FIXED>%s", pRootFixed->is_fixed ? "fixed" : "not fixed");
        break;
    }
    /**
     * Event called when there is another and best possible candidate to be root of the network;
     * The current root passes control to the new root to take over the network;
     */
    case MESH_EVENT_ROOT_ASKED_YIELD:
    {
        mesh_event_root_conflict_t *pRootConflict = (mesh_event_root_conflict_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_ROOT_ASKED_YIELD>" MACSTR ", rssi:%d, capacity:%d",
                 MAC2STR(pRootConflict->addr), pRootConflict->rssi, pRootConflict->capacity);
        break;
    }
    /**
     * Channel switch event
     */
    case MESH_EVENT_CHANNEL_SWITCH:
    {
        mesh_event_channel_switch_t *pChannelSwitch = (mesh_event_channel_switch_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_CHANNEL_SWITCH>new channel:%d", pChannelSwitch->channel);
        break;
    }

    /**
     * Scan is done event
     */
    case MESH_EVENT_SCAN_DONE:
    {
        mesh_event_scan_done_t *pScanDone = (mesh_event_scan_done_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_SCAN_DONE>number:%d", pScanDone->number);
        break;
    }
        /**
         * Mesh state event
         */
    case MESH_EVENT_NETWORK_STATE:
    {
        mesh_event_network_state_t *pNetworkState = (mesh_event_network_state_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_NETWORK_STATE>is_rootless:%d", pNetworkState->is_rootless);
        break;
    }
    case MESH_EVENT_STOP_RECONNECTION:
    {
        ESP_LOGI(TAG, "<MESH_EVENT_STOP_RECONNECTION>");
        break;
    }
    case MESH_EVENT_FIND_NETWORK:
    {
        mesh_event_find_network_t *pFindNetwork = (mesh_event_find_network_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_FIND_NETWORK>new channel:%d, router BSSID:" MACSTR "",
                 pFindNetwork->channel, MAC2STR(pFindNetwork->router_bssid));
        break;
    }
    case MESH_EVENT_ROUTER_SWITCH:
    {
        mesh_event_router_switch_t *pRouterSwitch = (mesh_event_router_switch_t *)pEventData;
        ESP_LOGI(TAG, "<MESH_EVENT_ROUTER_SWITCH>new router:%s, channel:%d, " MACSTR "", pRouterSwitch->ssid,
                 pRouterSwitch->channel, MAC2STR(pRouterSwitch->bssid));
        break;
    }
    break;
    default:
        ESP_LOGI(TAG, "unknown id:%ld", event_id);
        break;
    }
}

void ip_event_handler(void *arg, esp_event_base_t event_base,
                      int32_t event_id, void *event_data)
{
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    ESP_LOGI(TAG, "<IP_EVENT_STA_GOT_IP>IP:" IPSTR, IP2STR(&event->ip_info.ip));
    s_current_ip.addr = event->ip_info.ip.addr;
#if !CONFIG_MESH_USE_GLOBAL_DNS_IP
    esp_netif_t *netif = event->esp_netif;
    esp_netif_dns_info_t dns;
    ESP_ERROR_CHECK(esp_netif_get_dns_info(netif, ESP_NETIF_DNS_MAIN, &dns));
    mesh_netif_start_root_ap(esp_mesh_is_root(), dns.ip.u_addr.ip4.addr);
#endif
    esp_mesh_rx_start();
}

void mesh_app_start(void)
{
    ESP_ERROR_CHECK(esp_netif_init());
    /*  event init */
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(mesh_netifs_init(recv_cb));

    /*  wifi init */
    wifi_init_config_t config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&config));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &ip_event_handler, NULL));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
    ESP_ERROR_CHECK(esp_wifi_start());

    /*  mesh init */
    ESP_ERROR_CHECK(esp_mesh_init());
    ESP_ERROR_CHECK(esp_event_handler_register(MESH_EVENT, ESP_EVENT_ANY_ID, &MeshEventHandler, NULL));
    ESP_ERROR_CHECK(esp_mesh_set_max_layer(CONFIG_MESH_MAX_LAYER));
    ESP_ERROR_CHECK(esp_mesh_set_vote_percentage(1));
    ESP_ERROR_CHECK(esp_mesh_set_ap_assoc_expire(12));
    mesh_cfg_t cfg = MESH_INIT_CONFIG_DEFAULT();

    /* mesh ID */
    memcpy((uint8_t *)&cfg.mesh_id, MESH_ID, 6);

    /* router */
    cfg.channel = CONFIG_MESH_CHANNEL;
    cfg.router.ssid_len = strlen(CONFIG_MESH_ROUTER_SSID);
    memcpy((uint8_t *)&cfg.router.ssid, CONFIG_MESH_ROUTER_SSID, cfg.router.ssid_len);
    memcpy((uint8_t *)&cfg.router.password, CONFIG_MESH_ROUTER_PASSWD, strlen(CONFIG_MESH_ROUTER_PASSWD));

    /* mesh softAP */
    ESP_ERROR_CHECK(esp_mesh_set_ap_authmode(CONFIG_MESH_AP_AUTHMODE));
    cfg.mesh_ap.max_connection = CONFIG_MESH_AP_CONNECTIONS;
    memcpy((uint8_t *)&cfg.mesh_ap.password, CONFIG_MESH_AP_PASSWD, strlen(CONFIG_MESH_AP_PASSWD));
    ESP_ERROR_CHECK(esp_mesh_set_config(&cfg));
    ESP_ERROR_CHECK(esp_mesh_start());
}
