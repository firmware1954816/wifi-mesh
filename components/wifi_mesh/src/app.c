
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/gpio.h"

#include "app.h"
#include "mesh.h"
#include "mqtt_app.h"

#include "sys_config.h"
#include "esp_log.h"
#include "esp_mesh.h"
#include "esp_mesh_internal.h"
#include "esp_mac.h"


#include "lwip/err.h"
#include "lwip/sys.h"
#include <lwip/sockets.h>

extern EventGroupHandle_t wifi_event_group;
extern const int CONNECTED_BIT;
extern mesh_addr_t route_table[];
extern char mac_address_root_str[];

static const char *TAG = "app: ";

#define RX_SIZE (200)
#define TX_SIZE (200)

char mac_addr[50];
char mac_add[50];

#define NODE_NAME "NODE_D4"

void gpios_setup(void)
{
    gpio_config_t io_conf_output;
    io_conf_output.intr_type = GPIO_INTR_DISABLE;
    io_conf_output.mode = GPIO_MODE_OUTPUT;
    io_conf_output.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    io_conf_output.pull_down_en = 0;
    io_conf_output.pull_up_en = 1;
    gpio_config(&io_conf_output);

    gpio_set_level(ONBOARD_LED, 0);

    gpio_config_t io_conf_input;
    io_conf_input.intr_type = GPIO_INTR_DISABLE;
    io_conf_input.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    io_conf_input.mode = GPIO_MODE_INPUT;
    io_conf_input.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf_input.pull_up_en = GPIO_PULLUP_ENABLE;
    gpio_config(&io_conf_input);
}

void button_task(void *pvParameter)
{
    bool button_press= 0;
    bool last_press= 0;
    uint8_t tx_buf[TX_SIZE] = {
        0,
    };

    char mac_str[30];
    int route_table_size = 0;

    esp_err_t err;

    mesh_data_t data;
    data.data = tx_buf;
    data.size = TX_SIZE;
    data.proto = MESH_PROTO_BIN;

    MQTT_AppStart();

    while (1)
    {
        if (esp_mesh_is_root())
        {
            gpio_set_level(ONBOARD_LED, 1);
        }
        if (esp_mesh_is_root() || !esp_mesh_is_root())
        {
            button_press = !gpio_get_level(BUTTON);
            if (button_press)
            {
                if(!last_press)
                {
                    ESP_LOGI(TAG, "Button %d Pressed.\r\n", BUTTON);

                    esp_mesh_get_routing_table((mesh_addr_t *)&route_table,
                                                CONFIG_MESH_ROUTE_TABLE_SIZE * 6, &route_table_size);

                    xSemaphoreTake(s_route_table_lock, portMAX_DELAY);

                    for (int i = 0; i < route_table_size; i++)
                    {
                        sprintf(mac_str, MACSTR, MAC2STR(route_table[i].addr));
                        snprintf((char *)tx_buf, TX_SIZE, "Button Pressed  node with <MAC> %s  send data to Node with, <MAC> %s", mac_add, (mac_str));
                        data.size = strlen((char *)tx_buf) + 1;

                        // Internal communication Send to root & other Nodes except itself
                        ESP_LOGI(TAG,"mac_add------%s", mac_add);
                        ESP_LOGI(TAG,"routing table mac-----%s", mac_str);
                        if (strcmp(mac_add, mac_str) != 0)
                        {
                            err = esp_mesh_send(&route_table[i], &data, MESH_DATA_P2P, NULL, 0);
                            if (err)
                            {
                                ESP_LOGI(TAG, "ERROR : Sending Message!\r\n");
                            }
                            else
                            {
                                ESP_LOGI(TAG, "%s", (char *)tx_buf);
                            }
                        }
                        else
                        {
                            ESP_LOGE(TAG, "MAC OF SOURCE = MAC OF DESTINATION");
                        }
                    }

                    xSemaphoreGive(s_route_table_lock);
                    if (!esp_mesh_is_root())
                    {
                        MQTT_AppPublish(MQTT_BUTTON_TOPIC, mac_add);
                    }
                }
            }
        }
        last_press = button_press;
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

void task_mesh_rx(void *pvParameter)
{
    uint8_t rx_buf[RX_SIZE] = {
        0,
    };

    esp_err_t err;
    mesh_addr_t from;
    mesh_data_t data;
    data.data = rx_buf;
    data.size = RX_SIZE;

    char mac_address_str[30];
    int flag = 0;

    while (1)
    {
        data.size = RX_SIZE;
        err = esp_mesh_recv(&from, &data, portMAX_DELAY, &flag, NULL, 0);
        if (err != ESP_OK || !data.size)
        {
            ESP_LOGI(TAG, "err:0x%x, size:%d", err, data.size);
        }
        if (esp_mesh_is_root())
        {
            ESP_LOGI(TAG, "ROOT(MAC:%s) - Msg: %s, ", mac_address_root_str, data.data);
            snprintf(mac_address_str, sizeof(mac_address_str), "" MACSTR "", MAC2STR(from.addr));
            ESP_LOGI(TAG, "send by NON-ROOT: %s\r\n", mac_address_str);
        }

        else
        {
            ESP_LOGI(TAG, "NON-ROOT NODE WHICH RECV MSG: (MAC:%s)- Msg: %s, ", mac_addr, data.data);
            snprintf(mac_addr, sizeof(mac_addr), "" MACSTR "", MAC2STR(from.addr));
            ESP_LOGI(TAG, "Send by  WHICH ROOT: %s\r\n", mac_addr);
        }
    }

    vTaskDelete(NULL);
}

void getmac()
{
    uint8_t chipid[20];
    esp_efuse_mac_get_default(chipid);
    snprintf(mac_add, sizeof(mac_add), "" MACSTR "", MAC2STR(chipid));
    ESP_LOGD(TAG, "mac address of non root is %s\n\n\n", mac_add);
}

void task_app_create(void)
{
    getmac();

    xTaskCreate(task_mesh_rx, "task_mesh_rx", 1024 * 5, NULL, 2, NULL);

    xTaskCreate(button_task, "button_task", 1024 * 8, NULL, 1, NULL);
}
