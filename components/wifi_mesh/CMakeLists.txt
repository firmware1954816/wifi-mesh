idf_build_get_property(target IDF_TARGET)

idf_component_register(SRC_DIRS "src"
                     INCLUDE_DIRS "inc"
                     REQUIRES mqtt_comm esp_wifi nvs_flash driver
                     )